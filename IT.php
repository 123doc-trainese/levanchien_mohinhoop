<?php
require_once 'Model.php';
require_once 'Staff.php';

class IT extends Model implements Staff
{

    protected $table = 'tbl_IT';
    protected $attributes = [
        'id', 'name', 'salary', 'bonus'
    ];

    /**
     * Get salary of IT
     *
     * @return int
     */
    public function getSalary(): int
    {
        return ($this->salary ?: 0) + ($this->bonus ?: 0);
    }
}

